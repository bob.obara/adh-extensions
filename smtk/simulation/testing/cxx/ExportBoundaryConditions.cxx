//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/adh/ExportBoundaryConditions.h"

int ExportBoundaryConditions(int argc, char* argv[])
{
  smtk::simulation::adh::ExportBoundaryConditions exportBoundaryConditions;
  // TODO: test C++ export logic

  return 0;
}
